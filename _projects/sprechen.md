---
layout: page
title: Über den Tod sprechen
description: Kommunikation mit Kindern
img: assets/img/todsprechen.jpg
importance: 20
---

Wie sprechen wir über den Tod? Mein Eindruck: immer noch zu wenig und zu ängstlich. Hinter dem oft gehörten Satz „Der Tod gehört einfach zum Leben dazu“ steckt leider oft keine Substanz – wir sagen das, aber leben es nicht. Ich wünsche mir, dass sich Eltern mutig den Fragen ihrer Kinder stellen: „Muss ich denn auch mal sterben?“, „Hat der Opa nicht Angst im dunklen Sarg?“ oder ähnliche zunächst unangenehme Fragen. Das Schwierige und gleichzeitig das Erleichternde ist, dass es auf diese Fragen keine richtigen Antworten gibt. Ich möchte Ihnen helfen, neu an diese Themen heranzugehen, sodass daraus schöne, verbindende Gespräche zwischen Ihnen und Ihren Kindern entstehen. Aus meiner Sicht prägt es ein Kind auch langfristig auf günstige Weise, wenn die Bezugspersonen für solche Gespräche offen sind und vor allem ihre Angst davor allmählich abbauen. Und auch wir Erwachsenen können viel von unseren kleinen Gesprächspartnern lernen.

<center> <img src="{{ site.url }}{{ site.baseurl }}/assets/img/todsprechen.jpg" class="img-fluid z-depth2 rounded" style="width: 90%;  padding:10px"></center>