---
layout: page
title: Begleitung
description: bei schwerer Krankheit und darüber hinaus
img: assets/img/howareyou_s.jpg
importance: 30
category: work
---
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/howareyou.jpg" class="img-fluid z-depth2 float-right rounded" style="width: 45%;  padding:10px">
Die Diagnose einer tödlichen Krankheit bringt immer einen Umbruch mit sich – das Leben stellt sich auf den Kopf und drängen sich plötzlich existenzielle Fragen auf: Was hinterlasse ich an immateriellen Werten? Wie kann ich den Abschied von meinen Liebsten gestalten? Was möchte ich vor meinem Tod noch regeln? Als Sterbeamme begleite ich Sie in den Fällen, in denen Sie lieber mit einer neutralen Person über diese Dinge sprechen möchten.
Auch Angehörige eines sterbenden Menschen benötigen manchmal eine Begleitung: Wie bereitet man sich auf das Sterben und den Verlust vor? Welche Rituale sind geeignet, um sich zu verabschieden?

Auch Ängste kommen sowohl in der Trauer als auch im Sterbeprozess häufig vor. Ich bringe Ideen aus meiner Ausbildung mit, die Ängste mildern können.
