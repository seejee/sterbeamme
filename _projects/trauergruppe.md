---
layout: page
title: Trauergruppe "die Wolke"
description: für Kinder und Jugendliche
img: assets/img/wolke_s.jpeg
importance: 50
---
Nach fast einem Jahr Vorbereitungszeit leite ich seit September 2023 eine Kindertrauergruppe, die wir „die Wolke“ genannt haben.  Ermöglicht wurde dies durch den ambulanten Kinder- und Jugendhospizdienst der Malteser im Landkreis Waldshut.
Zuvor gab es in der Region kein vergleichbares Angebot und wir freuen uns, einen Beitrag zu leisten, um diese Lücke zu schließen. Die Zielgruppe sind Kinder und Jugendliche ab 6 Jahren. 
<center> <img src="{{ site.url }}{{ site.baseurl }}/assets/img/wolke.png" class="img-fluid z-depth2 rounded" style="width: 100%;  padding:0px"></center>