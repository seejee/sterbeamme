---
layout: page
title: Trauerbegleitung
description: für Kinder und Jugendliche
img: assets/img/kindstrauer.jpg
importance: 10
---

Kinder trauern oft anders als Erwachsene. Ich finde hier das Bild der sogenannten Pfützentrauer hilfreich: In einem Augenblick ist das Kind furchtbar traurig (oder anders emotional), im nächsten springt es aber sozusagen wieder aus der Trauerpfütze raus und will ein Eis essen, lachen und spielen - und das von einer Minute auf die andere. Wenn wir Erwachsene uns bewusst machen, dass das ganz normal ist, können wir leichter mit diesem Wechselbad der Gefühle umgehen.

Auch das Alter des Kindes bzw. des Jugendlichen spielt eine Rolle für die Art der Begleitung, weil das Verständnis für den Tod bei den Kleinsten natürlich anders ist als etwa bei Schulkindern.

Diese und weitere Besonderheiten gilt es bei Kindern in Trauer zu beachten. Meine Begleitung ist einfühlsam, altersangemessen, zugewandt und geprägt von einem nicht versiegenden Erstaunen über die Weisheit von Kindern. Neben Gesprächen mit dem Kind selbst können auch Gespräche mit den Eltern oder anderen Bezugspersonen günstig sein. Wünschenswert ist für mich, dass die Erwachsenen, die dem Kind am nächsten stehen, selbstbewusst, empathisch und geduldig mit der Trauer ihres Kindes umzugehen lernen – denn sie sind die Experten für ihr eigenes Kind. 
<center> <img src="{{ site.url }}{{ site.baseurl }}/assets/img/kindstrauer.jpg" class="img-fluid z-depth2 rounded" style="width: 90%;  padding:10px"></center>