---
layout: page
title: Trauerbegleitung
description: für Erwachsene
img: assets/img/kintsugi2.jpg
importance: 1
---
*Bedenkt: den eignen Tod, den stirbt man nur, doch mit dem Tod der andern muß man leben.*
(aus dem Gedicht „Memento“ von Mascha Kaléko)


Der Tod eines nahestehenden Menschen stellt für die meisten von uns einen Umbruch dar. Je nachdem, wie eng die Beziehung war und wie wir selbst gerade psychisch aufgestellt sind, ist der Umbruch größer oder kleiner.

In einem Umbruch steckt ein „Bruch“ – und lässt etwa an zerbrochenes Geschirr denken. Der Mensch in Trauer muss mühevoll und geduldig die Scherben wieder zusammensetzen – jedoch wird das Ergebnis niemals wieder ganz die ursprüngliche Form bekommen.

Ich kenne für diesen Prozess kein passenderes Bild als die japanische Technik des Kintsugi. Bei dieser traditionellen Reparaturtechnik für Keramik werden die Bruchlinien sogar hervorgehoben, indem sie u.a. mit Gold veredelt werden. Es entsteht etwas Neues, vielleicht sogar Schöneres, das aber die Brüche nicht verschweigt.

Das ist ein Beispiel dafür, welche Gedanken und Bildern wir bei der Trauerbegleitung nutzen können. Gemeinsam arbeiten wir heraus, was Sie in dieser herausfordernden Zeit benötigen.
 <center>
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/kintsugi2.jpg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/wolkenhimmel.jpg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:3">
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/kintsugi.jpeg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
</center>
