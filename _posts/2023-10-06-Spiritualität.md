---
layout: post
title:  Spiritualität
date: 2023-10-07 19:03:00
description: Glauben oder nicht glauben, das ist hier die Frage.
tags: Spiritualität
categories: 
thumbnail: assets/img/naone2.jpg
---
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/naone2.jpg" class="img-fluid z-depth2 float-right rounded" style="width: 45%;  padding:10px">

Vorab das Wichtigste: Als Sterbe- und Lebensamme begleite ich neutral, also unabhängig von Konfession oder Glaube. Mein Gegenüber gibt also die Richtung vor und bekommt von mir keine Weltanschauungen übergestülpt. Wenn Sie nicht religiös sind, nicht an Gott oder ein Jenseits glauben, dann gestalten wir unsere gemeinsame Arbeit auch entsprechend. Möchten Sie sich aber gern über Jesus Christus unterhalten, etwa weil Ihr Glaube Ihnen Trost und Kraft spendet, dann bin ich dank meiner christlich geprägten Erziehung relativ bibelfest. Bei anderen Religionen und Weltbildern bin ich sehr interessiert daran, mich zu informieren, um Ihnen eine passende Begleitung zu bieten.

Meine persönliche spirituelle Haltung ist maßgeblich beeinflusst durch die Schriften von [Heinz Grill](https://heinz-grill.de/) und Rudolf Steiner. Daher rührt auch meine tiefe Überzeugung, dass der Mensch einen Geist hat (und im Begriff "Spiritualität" steckt genau dieser "spiritus") und ein schöpferisches Wesen ist, das sich stets weiterentwickeln und etwas schaffen möchte. Im Rahmen meiner Tätigkeit bedeutet das, dass ich jeden Menschen, unabhängig von seinem Alter, als geistbegabt betrachte und ihm helfen möchte, seine Handlungsfähigkeit wiederzufinden oder auszubauen. Ich vertrete die Haltung, dass niemand nur Opfer seiner Umstände sein muss, auch wenn jeder von mal günstigen, mal schweren Umständen beeinflusst ist. 


