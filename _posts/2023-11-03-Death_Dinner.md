---
layout: post
title:  Der Tod beim Abendessen
date: 2023-11-03 19:03:00
description: Lässt sich beim Essen besser über den Tod sprechen?
tags: 
categories: 
thumbnail: assets/img/esstisch.jpg
---
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/esstisch.jpg" class="img-fluid z-depth2 float-right rounded" style="width: 30%;  padding:10px">

Was klingt wie ein zugegebenermaßen lahmer Titel für ein Krimi-Dinner, ist in Wirklichkeit eine Bewegung, die 2013 in den USA ins Leben gerufen wurde. Der US-Amerikaner Michael Hebb wurde auf einer Zugfahrt im Gespräch mit zwei fremden Mitreisenden, die Ärzte waren, zu der Vorstellung inspiriert, dass ein gemeinsames Abendessen die ideale Gelegenheit sei, um sich über die Themen Sterben, Tod und Trauer zu unterhalten. Die Idee für „Death over Dinner“ war geboren (in diesem [TED-Talk](https://www.youtube.com/watch?v=4DT0aMfFtuw) spricht Hebb darüber – leider nur auf Englisch). Den Ansatz finde ich gut nachvollziehbar: Gemeinsam am Tisch zu sitzen mit Freunden, Familie oder auch fremden Menschen und dabei gemütlich zu essen und zu trinken schafft eine vertraute, warme und tendenziell heitere Atmosphäre, in der man sich wohlfühlt und eher öffnen kann. Man könnte meinen, dass es die Stimmung komplett vermiesen würde, jetzt Themen wie Beerdigung, Trauergruppen oder Vorsorgevollmacht anzuschneiden – doch der Erfolg von „Death over Dinner“ scheint das zu widerlegen. Über [deathoverdinner.org](https://deathoverdinner.org/) können sich Menschen in den USA, die solch ein Dinner ausrichten möchten, mithilfe verschiedener Fragen eine Art Skript erstellen lassen, das sie dann den eingeladenen Gästen zukommen lassen und das auch als loser Fahrplan für den Abend dient. Seitdem es dieses kostenfreie Angebot gibt, wurden laut Website mehr als Hunderttausend solcher „Death Dinners“ veranstaltet.

Wie so ein Abendessen aussehen könnte, kann man sich in einer Reihe von [YouTube-Videos](https://www.youtube.com/watch?v=-7XJN1FrlGg) ansehen. Ein Kamerateam begleitete eine bunt gemischt Runde von Menschen bei ihrem Death Dinner. Der Gastgeber warf im Verlauf des Abendessens nach und nach einige Themen als Gesprächsanstöße auf, zu denen sich die Gäste äußern konnten. Das erste Thema war dem *Erinnern* gewidmet: Jeder bekam Gelegenheit, von einem seiner Verstorbenen zu sprechen und auf ihn anzustoßen. Anschließend tauschte man sich über das *Trauern* aus. Besonders vielschichtig fand ich, was die Gäste zum dritten Punkt *Leben nach dem Tod* zu sagen hatten – in der harmonischen Atmosphäre fanden ganz unterschiedliche Ansichten ihren Ausdruck. Das vierte und letzte Thema stellte die *Konfrontation mit der eigenen Sterblichkeit* dar.

Michael Hebb hebt einige positiven Folgen solcher Gesprächsrunden hervor: Wird ein Death Dinner zum Beispiel innerhalb einer Familie durchgeführt, lassen sich letzte Wünsche einfacher an die Angehörigen kommunizieren – selbst wenn das Sterben noch gar nicht im Raum steht. Denn nur wenn Menschen wagen, diese Wünsche zu äußern, kann an ihrem Lebensende auch danach gehandelt werden (dadurch, so Hebb, können zudem Einsparungen für das Gesundheitssystem entstehen, u.a. weil lebensverlängernde Maßnahmen eingestellt werden, die der Sterbende nicht mehr möchte). Auch seien solche Tischgespräche eine Möglichkeit, das Sprechen mit Ärzten, Pflegeheimleitungen und Bestattern zu üben, das häufig schwerfällt, aber das auf die meisten von uns irgendwann zukommt.

Was mich aber besonders interessiert, ist die Vorstellung, dass Gespräche während des Essens sogar an Qualität gewinnen, wenn solche existentiellen Themen… nun ja, auf den Tisch kommen. Es könnte eine neuartige Verbindung zwischen den Anwesenden schaffen, unabhängig davon, ob es sich nun um Unbekannte oder sehr eng Vertraute handelt. Und das Konzept wäre gut geeignet, den Tod ein Stückchen weiter [aus der Tabuzone](https://sterbeamme-lebensamme.de/aktuelles/2023/Anliegen/) zu ziehen, indem wir dieses Themenfeld in den Alltag einbinden.

Ich habe jedenfalls für den deutschsprachigen Raum keine vergleichbare Initiative gefunden. Doch lässt mich diese Idee jetzt seit Wochen nicht mehr los – wer wäre wohl bereit, der Einladung zu so einem Abendessen zu folgen?


**Update 05.11.2023:** Ein Leser hat die Frage aufgeworfen, ob es denn gleich ein Abendessen sein müsse – *Kaffee und Kuchen* wäre zum Ausprobieren vielleicht besser geeignet. Ein berechtigter Einwand, zumal wir das US-amerikanische Konzept wohl sowieso für uns Deutsche anpassen müssten. Kaffee und Kuchen scheint mir zu den typisch deutschen Arten des „Gemeinsam-Brotbrechens“ zu gehören.

