---
layout: post
title:  Mein Anliegen
date: 2023-10-06 19:02:00
description: Für welche Werte ich als Sterbe- und Lebensamme stehe.
tags: Grundsätzliches
categories: 
thumbnail: assets/img/spirale.jpg
---


## Raus aus der Sprachlosigkeit

Es wird nicht gern über den Tod geredet. Das merkt man spätestens, wenn man mal das Experiment wagt, bei einem gemütlichen Essen ohne Vorwarnung so etwas nachzufragen wie: „Sag mal, willst du eigentlich später verbrannt oder beerdigt werden?“ Gerade Kinder sind oft noch unvoreingenommen und stellen neugierige Fragen, für das den Erwachsenen manchmal die Worte, die Geduld und der Mut fehlen. Ich habe selbst im Laufe der letzten Jahre an mir gemerkt, wie mein Umgang mit dem Themenkomplex Sterben/Tod/Trauer einfacher und natürlicher wurde, weil ich viel darüber gesprochen habe. Ängste können so abgebaut werden. Und darüber hinaus ist es einfach ein spannendes Feld, das tiefgehende, erbauende Gespräche ermöglicht.


## Raus aus dem Tabu

Das Tabu rund ums Lebensende wird unter anderem deutlich an der räumlichen Trennung der Sterbenden von den Lebenden – egal, ob der Tod bei schwerer Krankheit schon bald ansteht oder sich beim normalen Alterungsprozess nach und nach nähert. Fast die Hälfte aller Menschen sterben im Krankenhaus, etwa ein Drittel im Seniorenheim, obwohl sich die meisten wünschen, [zu Hause zu sterben](https://faktencheck-gesundheit.de/fileadmin/files/Faktencheck/Diagramme/Palliativversorgung/Diagramm-Sterbeort-Wunsch-und-Wirklichkeit.jpg). Ist der Tod eingetreten, kümmert sich ein Bestattungsunternehmen um den Rest, während früher zum Beispiel Hausaufbahrungen, die Anwendung von Bräuchen und allgemein ein würdiger, für alle Beteiligten möglichst heilsamer Abschied viel häufiger anzutreffen waren.


## Raus aus der Isolation

Gemeinschaftliche und familiäre Strukturen im Umgang mit dem Tod gehen immer mehr verloren. Eine Sterbeamme setzt sich dafür ein, dass die betroffene Person (die sterben wird oder in Trauer ist) die Menschen in ihrem Umfeld, dem sogenannten „Dorf“, aktiviert, um deren Ressourcen zu nutzen. So kann jede und jede aus dem Dorf nach ihren oder seinen Kräften einen Beitrag leisten, um die Person in ihrer Krise zu unterstützen. Die Sterbeamme hat die einzelnen Akteure im Blick, geht bei Bedarf auch mit dem Dorf ins Gespräch und ist koordinierend und unterstützend tätig. Ziel ist es, dass sich Betroffene nicht mehr allein fühlen und sich das System Familie und Freundeskreis mehr oder weniger selbst tragen lernt.


## Kreative, individuelle Begleitung

Es gibt keine pauschalen Rezepte, wenn jemand schwer krank oder in Trauer ist, auch wenn wir das manchmal gerne so hätten. Haben Sie schon mal von Bullshit Bingo gehört? Ein wenig zynisch, aber durchaus nachvollziehbar sind bei diesem Bingo-Spiel floskelhafte Aussagen in eine Tabelle eingetragen, die Menschen in Krisen oft hören – wenn sie 5 Aussagen in einer Reihe gehört haben, haben sie "gewonnen" ([hier](https://iris-willecke.de/trauer-bullshit-bingo/) ein Beispiel für Trauer-Bullshit, [hier](https://carolionk.com/2018/11/27/diagnose-brustkrebs-dos-und-donts-fuer-angehoerige/) eines für Brustkrebs).


## Unentwegtes Lernen

Jede Begegnung, ob mit einem anderen Menschen, mit Tieren, der Natur oder bestimmten Schriften, birgt in sich die Möglichkeit zu lernen und sich weiterzuentwickeln. In diesem Sinne sind die Gespräche mit den Personen, die ich begleite, auch für mich Erfahrungs- und Lernquellen. Das trägt dazu bei, dass wir uns auf Augenhöhe unterhalten können. Mir ist es ein Anliegen, die Menschen, mit denen ich Kontakt habe, auch zu einem stetigen Lernen zu inspirieren.
 <center>
<img src="../../../assets/img/marienkaefer.jpeg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
<img src="../../../assets/img/gemeinschaft.jpg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:3">
<img src="../../../assets/img/spirale.jpg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
</center>
