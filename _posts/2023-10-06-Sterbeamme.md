---
layout: post
title:  Sterbeamme
date: 2023-10-05 19:01:00
description: Eine Sterbeamme beschäftigt sich tiefgehend mit dem Tod, dem Sterben, der Trauer und setzt sich für ein würdevolles, selbst bestimmtes Sterben ein.
tags: Grundsätzliches
categories: 
thumbnail: assets/img/Sterbeamme_diadelosmuertos.jpg
---

<img src="../../../assets/img/Sterbeamme_diadelosmuertos.jpg" class="img-fluid z-depth2 float-right rounded" style="width: 45%;  padding:10px" alt="Foto von Eduardo Dorantes auf Unsplash.com" caption="Foto von Eduardo Dorantes auf Unsplash.com" >


### Was ist eine Sterbeamme?

Um gleich ein häufiges Missverständnis auszuräumen: Eine Sterbeamme leistet keine Sterbehilfe, sondern sie begleitet den Prozess auf die Weise, die sich der Sterbende oder Trauernde wünscht.

Eine Sterbeamme beschäftigt sich tiefgehend mit dem Tod, dem Sterben, der Trauer und setzt sich für ein würdevolles, selbst bestimmtes Sterben ein. Sie verfügt über Werkzeuge, die sie Menschen im Sterbeprozess, den Angehörigen eines Sterbenden, Menschen in Trauer und allgemein in Krisen anbieten kann. Das vielleicht wichtigste Prinzip bei der Arbeit einer Sterbeamme lautet: Das Gegenüber ist der Experte. Das heißt, eine Sterbeamme hilft dem Menschen, den sie begleitet, seinen Umgang mit seiner Situation, seine Heilung, seine Antworten selbst zu finden – die eigenen Erfahrungen und das erworbene Wissen der Sterbeamme bieten höchstens den Hintergrund für die Begleitung. Eine Sterbeamme gibt keine Ratschläge, wohl aber hilft sie bei der Suche nach Ideen, gibt Anregungen, hält den Raum und ist einfach als neutrale Person da, damit das Gegenüber auch all das sagen und ausdrücken kann, was es bei nahestehenden Menschen oft nicht kann oder darf. Wenn Sie möchten, denken wir außerdem gemeinsam über den letzten Atemzug hinaus: Was erwartet uns nach dem Tod?



### Und wieso zusätzlich Lebensamme?

Ein würdiges Sterben ist wenig wert ohne ein würdiges, selbst bestimmtes Leben bis zum Schluss. In der Auseinandersetzung mit Fragen der Trauer und des Sterbens stellt sich auch immer die Frage nach den Gründen zum Leben, nach der Lebensfreude, dem Lebensfunken. Aufgabe der Sterbeamme ist es auch, die Lebensfreude (zum Beispiel in der Trauer) wieder herauszukitzeln zu versuchen und mit dem Gegenüber seine vielleicht momentan verdeckten Ressourcen wiederzuentdecken. Ist das nicht möglich, hilft die Sterbeamme bei der Vorbereitung auf die letzte große Reise und auf den Abschied.


