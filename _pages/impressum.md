---
layout: page
permalink: /impressum/
title: Impressum
description: 
nav: false
#nav_order: 5
---

### Verantwortlich (gem. § 55 Abs. 2 RStV und § 5 TMG)

Daniela Gehringer

Langackerweg

79790 Küssaberg

### Kontakt

[daniela@gehringer.li](mailto:daniela@gehringer.li)

+49 (0)151 56148568

### Datenschutzerklärung

Ich freue mich über Ihr Interesse an meiner Website. Datenschutz hat einen hohen Stellenwert für mich. Die Nutzung meiner Website ist in der Regel ohne Angabe personenbezogener Daten möglich. Es gibt jedoch Ausnahmen, über die ich Sie hiermit informieren möchte:

#### Cookies
Meine Website verwendet einen Cookie. Das ist eine kleine Datei, die von Ihrem Browser auf Ihrem Endgerät gespeichert wird. Mein Website verwendet lediglich einen Cookie, um Ihre Präferenz bezüglich eines hellen oder dunklen Themes zu speichern. Dieses Cookie enthält keine persönlichen Daten und wird ausschließlich zur Verbesserung der Benutzerfreundlichkeit verwendet.

### Server-Log-Dateien
Die Website wird auf gitlab.com gehostet. Ich habe keinen Zugriff auf die Server-Log-Dateien und auch keine Möglichkeit, diese Daten einzusehen oder zu verarbeiten. Für weitere Informationen zur Datenverarbeitung durch gitlab.com verweise ich auf die Datenschutzerklärung von GitLab.

Für weitere Fragen zum Datenschutz können Sie sich gerne direkt an mich wenden. Meine Kontaktinformationen finden Sie oben im Impressum.


