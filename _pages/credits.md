---
layout: page
permalink: /bildquellen/
title: Bildquellen
description: 
nav: false
#nav_order: 5
---
<p>Nicht hier aufgeführte Bilder sind eigene Bilder.</p>

<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/Sterbeamme_diadelosmuertos.jpg" class="img-fluid z-depth2  rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
    Foto von <a href="https://unsplash.com/de/@lalo2k?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Eduardo Dorantes</a> auf <a href="https://unsplash.com/de/fotos/UhE2lwGn-DQ?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>
<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/gemeinschaft.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
    Foto von <a href="https://unsplash.com/de/@dimhou?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Dim Hou</a> auf <a href="https://unsplash.com/de/fotos/2P6Q7_uiDr0?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>
<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/howareyou.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
    Foto von <a href="https://unsplash.com/de/@finnnyc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Finn</a> auf <a href="https://unsplash.com/de/fotos/nJupV3AOP-U?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>

<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/kintsugi2.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
Foto von <a href="https://unsplash.com/de/@riho_k?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Riho Kitagawa</a> auf <a href="https://unsplash.com/de/fotos/JuDPjcutors?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>

<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/kindstrauer.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
Foto von <a href="https://unsplash.com/de/@anniespratt?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Annie Spratt</a> auf <a href="https://unsplash.com/de/fotos/Pp2XOLkS5Jg?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>
<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/todsprechen.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
Foto von <a href="https://unsplash.com/de/@valeear?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Valeria Almaraz</a> auf <a href="https://unsplash.com/de/fotos/wu-u7pFL7HY?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  </figcaption>
</figure>
<figure class="figure">
 <img src="{{ site.url }}{{ site.baseurl }}/assets/img/esstisch.jpg" class="img-fluid z-depth2 rounded" style="width: 150px;  padding:10px" >
  <figcaption class="figure-caption text-end">
Foto von <a href="https://unsplash.com/de/@heftiba?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Toa Heftiba</a> auf <a href="https://unsplash.com/de/fotos/esstisch-mit-tellern-und-trinkglasern-16SJwUFgWCw?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
  
  </figcaption>
</figure>

  
  