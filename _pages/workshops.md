---
layout: workshops
permalink: /workshop/
title: Workshop
description:   <i>Hilfe, mein Kind stellt Fragen über den Tod</i>
nav: true
nav_order: 4
---

<table style="width:100%">
  <tr>
    <th style="width:60%"><p>Mögliche Termine</p>
	Sonntag, 25. Februar, am Vormittag oder Nachmittag<br>
	Samstag, 2. März, am Vormittag oder Nachmittag<br>
	Sonntag, 3. März, am Nachmittag<br>
	Sonntag, 10. März, am Vormittag oder Nachmittag<br>
	Weitere Termine auf Anfrage.
	</th>
    <th style="width:40%">   
    	<img src="{{ site.url }}{{ site.baseurl }}/assets/img/kindimgarten.jpeg" class="img-fluid z-depth2 rounded" style="width: 100%;  padding:2">
    </th>
 </tr>
 </table>

Schon kleine Kinder begegnen dem Tod in irgendeiner Form, sei es anhand der toten Feldmaus am Straßenrand oder des zerdrückten Käfers. Sie gehen meist anfangs sehr unbefangen damit um. Mit steigendem Alter kommen viele Fragen auf, die wir als Erwachsene zögerlich, wenn überhaupt, zu beantworten wagen. Als Vorbereitung auf andere, schwerwiegendere Todesfälle ist es aber günstig, sich dem Thema Tod gemeinsam mit dem Kind anzunähern. Wer das Wagnis eingeht, sich mit dem Tod, dem Sterben und der Trauer zu beschäftigen, wird nach meiner Erfahrung mit einem spannenden Austausch mit dem eigenen Kind und einer offenen, vertrauensvollen Kommunikation mit der Familie belohnt und bereitet sich und das Kind auf kommende Todesfälle vor. Zudem können wir Erwachsene auch aus der unschuldigen Weisheit unserer Kinder lernen und uns durch ihre Fragen inspirieren lassen, selbst über unsere Haltung zu diesem Thema nachzudenken.


Mein Ziel ist es, andere Erwachsene dabei zu unterstützen, dieses (lebens)wichtige Thema ganz praktisch, authentisch und mit einer gewissen Leichtigkeit in den Alltag einfließen zu lassen. Ich möchte auf diese Weise dazu beitragen, dass unsere Kinder von klein auf einen offenen, natürlichen Umgang mit dem Tod erlernen. Vielleicht kann die Beschäftigung mit großen und kleinen Verlusten dazu beitragen, dass wir alle widerstandsfähiger und gelassener in Krisen werden.


Der Workshop „HILFE, mein Kind stellt Fragen über den Tod“ soll ein Einstieg in dieses umfassende Thema sein. In gemütlicher Wohnzimmeratmosphäre und in kleinem Kreis möchte ich nützliche Informationen vermitteln. Besonders wichtig ist mir aber, dass wir gemeinsam eure persönlichen Fragen, Erfahrungen und Befürchtungen besprechen, damit die Theorie direkt an unsere Lebenspraxis mit unseren Kindern anknüpft.


Mögliche Aspekte, die wir angehen können (in Absprache mit den Teilnehmern):
- Mit tausend kleinen Tödchen üben: Wo begegnen wir dem Tod ganz natürlicherweise im Alltag?
- „Papa, du sollst sterben!“. Was verstehen Kinder unterschiedlichen Alters unter „tot“?
- Himmel, Hölle, Staub: Jenseitsvorstellungen von Kindern. Und Erwachsenen.
- Von zuckerwatteweich bis brutal ehrlich: Was kann/darf/soll ich meinen Kindern zumuten?
- Praktische Tipps: Worauf kann man achten, wenn man Kindern über die großen Fragen des Lebens spricht?
- Das Materielle: Welche Bücher und andere Hilfsmittel kann man zur Unterstützung heranziehen?
- Und wir Großen? Wie denken wir selbst eigentlich über den Tod, das „Danach“, aber auch über das „Davor“?

Kosten: 40 EUR pro Person (inklusive Getränke und Snacks)

<center>
	<a href="{{ site.url }}{{ site.baseurl }}/assets/pdf/Flyer_Hilfe.pdf">
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/flyer.png" class="img-fluid z-depth2 rounded" style="width: 35%;  padding:2"></a>
</center>
