---
layout: about
title: Start
permalink: /
subtitle:  
profile:
  align: right
  image: logo.png
  image_circular: false # crops the image to make it circular
  more_info: 
news: false  # includes a list of news items
latest_posts: true  # includes a list of the newest posts
social: false  # includes social icons at the bottom of the page
---
<div class="float-right">
<img src="assets/img/logo.png" alt="Sterbeamme Logo" width="250" class="img-fluid  z-depth0 float-right"><br/>
<div class="more-info-text float-right">
<b>Kontakt:</b> <br>
<a href="mailto:daniela@gehringer.li">daniela@gehringer.li</a><br>
+49 (0)151 56148568</div></div>


<p>&nbsp;</p>

Eine Hebamme wird bekanntermaßen zu Beginn eines Lebens aktiv.

Eine Sterbeamme am Ende.

Nicht nur das **Sterben** an sich kann sie begleiten, sondern auch die **Vorbereitung** darauf, die **Trauer** sowohl vor als auch nach dem Tod, und auch die **allgemeine Auseinandersetzung** mit diesen Themen schon ab Kindesalter kann sie ermutigen und mitgestalten. 



Als zertifizierte Sterbeamme nach Claudia Cardinal biete ich diese Art von Begleitung für Kinder und Erwachsene an: Wenn Sie selbst [schwer erkrankt](../projects/krankheit/) sind, bei der [Trauer](../projects/erwachsene) über einen Todesfall Unterstützung benötigen, mit Kindern angemessen [über den Tod sprechen](../projects/sprechen/) möchten oder ein [Kind oder einen Jugendlichen in Trauer](../projects/kinder/) kennen, schreiben Sie mir gern eine E-Mail oder rufen Sie mich an. 

Bei einem unverbindlichen Telefongespräch können wir gemeinsam herausfinden, welche Art von Begleitung Sie benötigen. Außerdem kläre ich Sie über die entstehenden Kosten auf.

Unter [Aktuelles](../aktuelles/) erfahren Sie mehr über die Arbeit einer Sterbe- und Lebensamme, über mein persönliches Anliegen bei meiner Tätigkeit und weiteres mehr.

Ihre
Daniela Gehringer







 

