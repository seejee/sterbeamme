---
layout: page
permalink: /uebermich/
title: Über mich
description: 
nav: true
nav_order: 5
---
Wie ich dazu gekommen bin, Sterbeamme zu werden, ist vielleicht etwas untypisch, denn ich habe vorher weder privat noch beruflich mit diesem Bereich zu tun gehabt. Als sprachbegeisterter Mensch habe ich jahrelang als Übersetzerin gearbeitet, und das sehr gern.

Dann kam Corona, und wie für viele andere markierte diese Zeit einen Übergang, eine Art Grenzerfahrung: Die Themen Krankheit und Tod waren plötzlich allgegenwärtig und der schwierige Umgang damit ebenso. Es hat mich sehr betroffen gemacht, dass viele Menschen in dieser Zeit nicht bei ihren kranken oder sterbenden Angehörigen sein konnten und ein würdiger Abschied oft unmöglich war. In einem Podcast hörte ich das erste Mal Claudia Cardinal über das Thema Tod sprechen. Sie ist die Gründerin der [Sterbeakademie](https://sterbeamme.de/), bei der ich ein paar Monate später meine Ausbildung zur Sterbeamme beginnen sollte. Ihre Herangehensweise an die Themen Sterben, Tod und Trauer haben mich von Anfang an fasziniert und ich empfand sie als große Bereicherung, an der ich auch andere Menschen teilhaben lassen wollte. Was mich außerdem schon seit Jahren beschäftigt, sind die existenziellen, spirituellen Fragen, die sich wohl die meisten früher oder später stellen: Woher kommen wir? Wohin gehen wir? Sind wir mehr als unser Körper? (Spoiler Alert: Ja, glaube ich ziemlich sicher!) Warum sind wir da?

Seit 2021 dringe ich immer mehr in dieses Interessensfeld ein: Neben meiner zweijährigen Sterbeammenausbildung habe ich mich unter anderem mit Bestattern unterhalten, mit Anbietern von Grabsteinen, Pflegekräften und Ärzten. Spannend und lehrreich sind aber auch die Gespräche mit meinem 4-jährigen Sohn und der Austausch mit anderen Eltern, mit Trauernden im Umfeld und mit meiner Familie über Erfahrungen mit unseren persönlichen Todesfällen. Es hat sich herauskristallisiert, dass ich beruflich Menschen begleiten möchte, die auf irgendeine Weise mit Trauer und Tod konfrontiert sind. Dabei ist mir die Arbeit mit Kindern und Jugendlichen ein besonderes Anliegen, denn ein natürlicher, angstfreier Umgang mit diesen Themen kann – so meine Hoffnung – dazu beitragen, dass aus ihnen resiliente, lebensbejahende Erwachsene werden, die wiederum eine Kultur, die den Tod  auf angemessene Weise zurück ins Leben holt, fördern können.

So habe ich mein Leben um einen großen Forschungs- und Erfahrungsbereich erweitert. Erhalten geblieben ist mir die Leidenschaft für Sprache, die nun auch in der Begleitung von Menschen zum Tragen kommt. Außerdem begeistert mich die Gartenarbeit und die Hobbyhühnerhaltung – übrigens auch Bereiche, in denen es viel um das Werden und Vergehen, um das Leben und den Tod geht. 

 <center>
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/dasbinich.jpeg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/coronasteine.jpeg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:3">
<img src="{{ site.url }}{{ site.baseurl }}/assets/img/zwetchgen.jpeg" class="img-fluid z-depth2 rounded" style="width: 31%;  padding:2">
</center>
